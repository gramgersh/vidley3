﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidley3.Models;
using Vidley3.ViewModels;
using System.Data.Entity;

namespace Vidley3.Controllers
{
    public class CustomerController : Controller
    {
        private ApplicationDbContext _context;

        public CustomerController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }

        // GET: Customer
        public ActionResult Index()
        {
            var customers = _context.Customers.Include(c => c.MembershipType).ToList();
            return View(customers);
        }

        public ActionResult New ()
        {
            CustomerViewModel viewModel = new CustomerViewModel()
            {
                MembershipTypes = _context.MembershipTypes.ToList()
            };
            return View("CustomerForm",viewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("New", "Customer");

            Customer customer = _context.Customers.SingleOrDefault(m => m.Id == (int)id);

            if (customer == null)
                return RedirectToAction("New", "Customer");

            CustomerViewModel viewModel = new CustomerViewModel(customer)
            {
                MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer)
        {
            if ( ! ModelState.IsValid )
            {
                CustomerViewModel viewModel = new CustomerViewModel(customer)
                {
                    MembershipTypes = _context.MembershipTypes.ToList()
                };
                return View("CustomerForm", viewModel);
            }

            Customer curCustomer = _context.Customers.SingleOrDefault(m => m.Id == customer.Id);
            if ( curCustomer == null )
            {
                _context.Customers.Add(customer);
            }
            else
            {
                curCustomer.Name = customer.Name;
                curCustomer.MembershipTypeId = customer.MembershipTypeId;
                curCustomer.BirthDate = customer.BirthDate;
                curCustomer.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;
            }

            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Detail(int? id)
        {
            if (id == null)
                return View();

            Customer customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);
            return View(customer);
        }
    }
}