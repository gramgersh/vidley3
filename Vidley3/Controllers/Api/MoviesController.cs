﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidley3.Models;
using Vidley3.DTOs;
using AutoMapper;

namespace Vidley3.Controllers.Api
{
    public class MoviesController : ApiController
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }

        // GET /api/Movies
        public IEnumerable<MovieDto> GetMovies()
        {
            return _context.Movies.ToList().Select(Mapper.Map<Movie, MovieDto>);
        }

        // GET /api/Movies/id
        public IHttpActionResult GetMovie(int id)
        {
            Movie movie = _context.Movies.SingleOrDefault(m => m.Id == id);
            if (movie == null)
                return NotFound();

            return Ok(Mapper.Map<Movie, MovieDto>(movie));
        }

        // PUT /api/Movies/id
        [HttpPut]
        public IHttpActionResult UpdateMovie(int id, MovieDto movieDto)
        {
            Movie curMovie = _context.Movies.SingleOrDefault(m => m.Id == id);
            if (curMovie == null)
                return NotFound();

            Mapper.Map(movieDto, curMovie);
            _context.SaveChanges();

            return Ok(movieDto);
        }

        // POST /api/Movies
        [HttpPost]
        public IHttpActionResult CreateMovie(MovieDto movieDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            Movie newMovie = new Movie();
            Mapper.Map(movieDto, newMovie);

            _context.Movies.Add(newMovie);
            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + newMovie.Id), Mapper.Map<Movie, MovieDto>(newMovie));
        }
        
        // DELETE /api/Movies/id
        public IHttpActionResult DeleteMovie(int id)
        {
            Movie movie = _context.Movies.SingleOrDefault(m => m.Id == id);
            if (movie == null)
                return NotFound();

            _context.Movies.Remove(movie);
            _context.SaveChanges();

            return Ok();
        }

    }
}
