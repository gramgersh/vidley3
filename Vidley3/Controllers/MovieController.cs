﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidley3.Models;
using Vidley3.ViewModels;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace Vidley3.Controllers
{
    public class MovieController : Controller
    {
        private ApplicationDbContext _context;

        public MovieController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult New()
        {
            MovieViewModel movieViewModel = new MovieViewModel()
            {
                Genres = _context.Genres.ToList(),
            };
            return View("MovieForm", movieViewModel);

        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("New", "Movie");

            Movie movie = _context.Movies.Single(m => m.Id == (int)id);

            if (movie == null)
                return RedirectToAction("New", "Movie");

            MovieViewModel movieViewModel = new MovieViewModel(movie)
            {
                Genres = _context.Genres.ToList(),
            };
            return View("MovieForm", movieViewModel);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Movie movie)
        {
            if (!ModelState.IsValid)
            {

                MovieViewModel viewModel = new MovieViewModel(movie)
                {
                    Genres = _context.Genres.ToList()
                };
                return View("MovieForm", viewModel);

            }
            Movie curMovie = _context.Movies.SingleOrDefault(m => m.Id == movie.Id);
            if (curMovie == null)
            {
                movie.DateAdded = (DateTime)DateTime.Now.Date;
                _context.Movies.Add(movie);
            }
            else
            {
                curMovie.Name = movie.Name;
                curMovie.GenreId = movie.GenreId;
                curMovie.ReleaseDate = (DateTime)movie.ReleaseDate;
                curMovie.NumberInStock = movie.NumberInStock;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Movie");
        }
        // GET: Movie
        public ActionResult Index()
        {
            // the ToList() causes the query to be made.
            var movies = _context.Movies.ToList();

            return View(movies);
        }

        // GET: Movie/Detail
        public ActionResult Detail(int? id)
        {
            if (id == null)
                return View();

            Movie movie = _context.Movies.Include(m => m.Genre).SingleOrDefault(m => m.Id == id);
            return View(movie);
        }
    }
}