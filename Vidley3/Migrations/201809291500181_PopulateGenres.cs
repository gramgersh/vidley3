namespace Vidley3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateGenres : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Genres (Name) VALUES ('Action')");
            Sql("INSERT INTO Genres (Name) VALUES ('Comedy')");
            Sql("INSERT INTO Genres (Name) VALUES ('Drama')");
            Sql("INSERT INTO Genres (Name) VALUES ('Romance')");
            Sql("INSERT INTO Genres (Name) VALUES ('Family')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Genres WHERE Name = 'Action'");
            Sql("DELETE FROM Genres WHERE Name = 'Comedy'");
            Sql("DELETE FROM Genres WHERE Name = 'Drama'");
            Sql("DELETE FROM Genres WHERE Name = 'Romance'");
            Sql("DELETE FROM Genres WHERE Name = 'Family'");
        }
    }
}
