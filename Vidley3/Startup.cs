﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Vidley3.Startup))]
namespace Vidley3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
