﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidley3.Models;
using System.ComponentModel.DataAnnotations;

namespace Vidley3.ViewModels
{
    public class CustomerViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }
        // Refactored Customer object

        public int? Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Display(Name = "Is Subscribed to Newsletter?")]
        public bool IsSubscribedToNewsletter { get; set; }

        [Required]
        [Display(Name = "Membership Type")]
        public int? MembershipTypeId { get; set; }

        [Display(Name = "Birth Date")]
        [ValidateMustBe18]
        public DateTime? BirthDate { get; set; }

        public CustomerViewModel()
        {
            Id = 0;
        }

        public CustomerViewModel(Customer customer)
        {
            Id = customer.Id;
            Name = customer.Name;
            MembershipTypeId = customer.MembershipTypeId;
            BirthDate = customer.BirthDate;
        }

        public string Title
        {
            get
            {
                return Id == null ? "New Customer" : "Edit Customer";
            }
        }
    }
}