﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vidley3.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Display(Name = "Is Subscribed to Newsletter?")]
        public bool IsSubscribedToNewsletter { get; set; }
        
        public MembershipType MembershipType { get; set; }

        [Required]
        [Display(Name = "Membership Type")]
        public int MembershipTypeId { get; set; }

        [Display(Name = "Birth Date")]
        [ValidateMustBe18]
        public DateTime? BirthDate { get; set; }

        // These are static properties that were populated via a Migration with
        // specific IDs
        public static int UnknownMT = 0;
        public static int PayAsYouGoMT = 1;

    }
}