﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vidley3.Models
{
    public class Genre
    {
        public short Id { get; set; }

        [Required]
        [StringLength(40)]
        public string Name { get; set; }
    }
}