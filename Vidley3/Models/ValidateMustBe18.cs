﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vidley3.Models
{
    public class ValidateMustBe18 : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Customer customer = (Customer)validationContext.ObjectInstance;
            // If the membership type is unknown or PayAsYouGo, then the birthdate is valid no matter what
            if (customer.MembershipTypeId == Customer.UnknownMT ||
                customer.MembershipTypeId == Customer.PayAsYouGoMT)
                return ValidationResult.Success;

            // Otherwise, they must be 18.
            if (customer.BirthDate == null)
                return new ValidationResult("Birthdate is required.");

            var age = DateTime.Today.Year - customer.BirthDate.Value.Year;

            if (age >= 18)
                return ValidationResult.Success;

            return new ValidationResult("Must be at least 18 to have this membership type.");
        }

    }
}